﻿using Domain.Exceptions;
using FluentAssertions;
using System;
using System.Linq;
using Test.Helpers;
using Xunit;

namespace Test.Domain
{
    public class SpotTest
    {
        [Fact(DisplayName = "Empty spot")]
        [Trait("Domain", "Spot")]
        public void Domain_Spot_Schedule_EmptySpot()
        {
            // Arrange

            var spot = MockHelper.GetSpot(1);

            // Act

            var vehicle = MockHelper.GetVehicleFaker().Generate();
            Action bookAction =  () => spot.Book(vehicle, DateTime.UtcNow.AddHours(1), DateTime.UtcNow.AddDays(1));

            // Assert

            bookAction.Should().NotThrow();
            spot.Bookings.Should().HaveCount(1);
            spot.Bookings.Any(i => i.VehicleId == vehicle.Id).Should().BeTrue();
            vehicle.Bookings.Any(i => i.SpotId == spot.Id).Should().BeTrue();
        }

        [Fact(DisplayName = "Try to schedule twice")]
        [Trait("Domain", "Spot")]
        public void Domain_Spot_Schedule_ScheduleTwice_ShouldThrowDomainException()
        {
            // Arrange

            var spot = MockHelper.GetSpot(1);
            var startDate = DateTime.UtcNow.AddMinutes(3);
            var endDate = DateTime.UtcNow.AddMinutes(50);

            // Act

            var vehicle = MockHelper.GetVehicleFaker().Generate();
            Action firstSchedulement = () => spot.Book(vehicle, startDate, endDate);
            Action secondSchedulement = () => spot.Book(vehicle, startDate, endDate);

            // Assert

            // Function
            firstSchedulement.Should().NotThrow();
            secondSchedulement.Should().Throw<DomainException>();

            // Outcome
            spot.Bookings.Should().HaveCount(1);
            spot.Bookings.Any(i => i.VehicleId == vehicle.Id).Should().BeTrue();

            vehicle.Bookings.Any(i => i.SpotId == spot.Id).Should().BeTrue();
        }

        [Fact(DisplayName = "Try to schedule in a full spot")]
        [Trait("Domain", "Spot")]
        public void Domain_Spot_Schedule_ScheduleFullSpot_ShouldThrowDomainException()
        {
            // Arrange

            var spot = MockHelper.GetSpot(1);
            var startDate = DateTime.UtcNow.AddMinutes(7);
            var endDate = DateTime.UtcNow.AddMinutes(21);

            // Act

            var vehicles = MockHelper.GetVehicleFaker().Generate(2);

            Action firstSchedulement = () => spot.Book(vehicles.ElementAtOrDefault(0), startDate, endDate);
            Action secondSchedulement = () => spot.Book(vehicles.ElementAtOrDefault(1), startDate, endDate.AddHours(1));

            // Assert

            firstSchedulement.Should().NotThrow();
            secondSchedulement.Should().Throw<DomainException>();

            spot.Bookings.Should().HaveCount(1);
            spot.Bookings.Any(i => i.VehicleId == vehicles.ElementAtOrDefault(0).Id).Should().BeTrue();
            spot.Bookings.Any(i => i.VehicleId == vehicles.ElementAtOrDefault(1).Id).Should().BeFalse();

            vehicles.ElementAtOrDefault(0).Bookings.Any(i => i.SpotId == spot.Id).Should().BeTrue();
            vehicles.ElementAtOrDefault(1).Bookings.Any(i => i.SpotId == spot.Id).Should().BeFalse();
        }

    }
}
