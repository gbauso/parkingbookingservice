﻿using CrossCutting.Exceptions;
using Domain.Exceptions;
using Domain.Service;
using FluentAssertions;
using System;
using System.Threading.Tasks;
using Test.Helpers;
using Xunit;

namespace Test.Domain
{
    public class ParkScheduleCommandHandlerTest
    {
        [Theory(DisplayName = "Valid Command")]
        [Trait("Command", "Spot")]
        [InlineData(true ,"ABC-123", 20, 50)]
        [InlineData(true, "ADC-133", 10, 120)]
        [InlineData(true, "CDC-31321", 5, 15)]
        public void ParkingScheduleCommand_Validate_ValidCommand(bool generateValidGuid,
                                                                        string plate,
                                                                        int minutesStart,
                                                                        int minutesEnd)
        {
            var id = generateValidGuid ? Guid.NewGuid() : Guid.Empty;
            var command = new ParkingScheduleCommand()
            {
                SpotId = id,
                VehiclePlate = plate,
                Start = DateTime.UtcNow.AddMinutes(minutesStart),
                End = DateTime.UtcNow.AddMinutes(minutesEnd)
            };

            Action validation = () => command.Validate().Wait();
            validation.Should().NotThrow();
        }

        [Theory(DisplayName = "Invalid Command")]
        [Trait("Command", "Spot")]
        [InlineData(true, "", 20, 50)]
        [InlineData(false, "ADC-133", 10, 120)]
        [InlineData(true, "ADC-133", -10, 120)]
        [InlineData(true, "CDC-31321", 5, 3)]
        public void ParkingScheduleCommand_Validate_InvalidCommand(bool generateValidGuid,
                                                                        string plate,
                                                                        int minutesStart,
                                                                        int minutesEnd)
        {
            var id = generateValidGuid ? Guid.NewGuid() : Guid.Empty;
            var command = new ParkingScheduleCommand()
            {
                SpotId = id,
                VehiclePlate = plate,
                Start = DateTime.UtcNow.AddMinutes(minutesStart),
                End = DateTime.UtcNow.AddMinutes(minutesEnd)
            };

            Action validation = () => command.Validate().Wait();

            validation.Should().Throw<ValidationException>();
        }

        [Fact(DisplayName = "New Vehicle")]
        [Trait("Command", "Spot")]
        public async Task ParkingScheduleCommandHandler_Handle_NewVehicle()
        {
            // Arrange
            var spotId = Guid.NewGuid();
            var handler = MockHelper.GetRequestHandler(spotId);

            var command = new ParkingScheduleCommand()
            {
                SpotId = spotId,
                VehiclePlate = "XYX-2120",
                Start = DateTime.UtcNow.AddMinutes(5),
                End = DateTime.UtcNow.AddMinutes(8)
            };

            // Act

            var result = await handler.Handle(command, default);

            // Assert
            result.Should().NotBeEmpty();
        }

        [Fact(DisplayName = "Double Booking Request")]
        [Trait("Command", "Spot")]
        public void ParkingScheduleCommandHandler_Handle_DoubleApplication()
        {
            // Arrange
            var spotId = Guid.NewGuid();
            var bmwId = Guid.NewGuid();

            var handler = MockHelper.GetRequestHandler(spotId, bmwId);

            var command = new ParkingScheduleCommand()
            {
                SpotId = spotId,
                VehiclePlate = "ABCD-123",
                Start = DateTime.UtcNow,
                End = DateTime.UtcNow.AddMinutes(50)
            };

            // Act

            Action handle = () => handler.Handle(command, default).Wait();

            // Assert
            handle.Should().Throw<DoubleSchedulingException>();
        }

        [Fact(DisplayName = "Spot Full at that time")]
        [Trait("Command", "Spot")]
        public void ParkingScheduleCommandHandler_Handle_SpotFull()
        {
            // Arrange
            var spotId = Guid.NewGuid();

            var handler = MockHelper.GetRequestHandler(spotId, capacity: 1);

            var command = new ParkingScheduleCommand()
            {
                SpotId = spotId,
                VehiclePlate = "ABCD-123",
                Start = DateTime.UtcNow.AddMinutes(3),
                End = DateTime.UtcNow.AddMinutes(50)
            };

            // Act

            Action handle = () => handler.Handle(command, default).Wait();

            // Assert
            handle.Should().Throw<SpotFullException>();
        }

        [Fact(DisplayName = "Inexistent Spot")]
        [Trait("Command", "Spot")]
        public void ParkingScheduleCommandHandler_Handle_InexistentSpot()
        {
            // Arrange
            var spotId = Guid.NewGuid();

            var handler = MockHelper.GetRequestHandler();

            var command = new ParkingScheduleCommand()
            {
                SpotId = spotId,
                VehiclePlate = "ABCD-123",
                Start = DateTime.UtcNow,
                End = DateTime.UtcNow.AddMinutes(50)
            };

            // Act

            Action handle = () => handler.Handle(command, default).Wait();

            // Assert
            handle.Should().Throw<ElementNotFoundException>();
        }
    }
}
