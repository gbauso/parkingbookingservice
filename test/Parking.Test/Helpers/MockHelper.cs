﻿using Bogus;
using CrossCutting.ServiceBus;
using Domain.Interfaces;
using Domain.Model;
using Domain.Service;
using MediatR;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Helpers
{
    public class MockHelper
    {
        public static Guid BmwId => Guid.NewGuid(); 


        public static Faker<Vehicle> GetVehicleFaker()
        {
            var faker = new Faker<Vehicle>()
                                    .CustomInstantiator(f => new Vehicle(f.Vehicle.Vin()));

            return faker;
        }

        public static Faker<Spot> GetSpotFaker(int? capacity = null)
        {
            var spotFaker = new Faker<Spot>()
                                    .CustomInstantiator(f => new Spot(
                                        f.Address.FullAddress(),
                                        capacity ?? f.Random.Int())
                                    );

            return spotFaker;

        }

        public static Spot GetSpot(int capacity = 3)
        {
            var spot = GetSpotFaker(capacity).Generate();

            return spot;
        }

        public static IQueuePublisher GetMockPublisher()
        {
            var mock = new Mock<IQueuePublisher>();

            return mock.Object;
        }

        public static IRequestHandler<ParkingScheduleCommand, Guid> GetRequestHandler(Guid? customSpot = null,
                                                                                       Guid? bmwId = null,
                                                                                       int capacity = 2)
        {
            var handler = new ParkingScheduleCommandHandler(GetVehicleRepository(bmwId),
                                                             GetSpotRepository(customSpot, bmwId, capacity),
                                                             GetUnitOfWork(),
                                                             GetMockPublisher());

            return handler;
        }

        private static IVehicleRepository GetVehicleRepository(Guid? bmwId = null)
        {
            var mock = new Mock<IVehicleRepository>();

            var studentList = GetVehicleFaker().Generate(5);
            studentList.Add(new Vehicle(bmwId ?? BmwId, "ABCD-123"));

            mock.Setup(o => o.FindVehicle(It.IsAny<string>()))
                        .Returns<string>(
                                    (plate) =>
                                                Task.FromResult(
                                                    studentList
                                                        .FirstOrDefault(i => i.Plate.ToLower() == plate.ToLower())
                                                        )
                                                    );

            mock.Setup(o => o.Add(It.IsAny<Vehicle>())).Callback<Vehicle>((student) => studentList.Add(student));

            return mock.Object;
        }

        private static ISpotRepository GetSpotRepository(Guid? customSpot = null,
                                                             Guid? bmwId = null,
                                                             int capacity = 2)
        {
            var mock = new Mock<ISpotRepository>();
            var spotList = GetSpotFaker().Generate(2);

            spotList.Add(new Spot(customSpot ?? Guid.NewGuid(),
                                      "Martin Avenue",
                                      capacity)
                           );

            mock.Setup(o => o.GetSpotInfo(It.Is<Guid>(i => !customSpot.HasValue)))
                .Returns<Guid>((id) =>
                {
                    var spot = spotList.FirstOrDefault(i => i.Id == id);
                    return Task.FromResult(spot);
                }
                );

            mock.Setup(o => o.GetSpotInfo(It.Is<Guid>(i => customSpot.HasValue)))
                .Returns<Guid>((id) =>
                {
                    var spot = spotList.FirstOrDefault(i => i.Id == id);
                    
                    spot.Book(new Vehicle(bmwId ?? BmwId, "ABCD-123"),
                                    DateTime.UtcNow.AddMinutes(2),
                                    DateTime.UtcNow.AddHours(2));

                    return Task.FromResult(spot);
                });


            return mock.Object;
        }

        private static IUnitOfWork GetUnitOfWork()
        {
            var mock = new Mock<IUnitOfWork>();

            return mock.Object;
        }

    }
}
