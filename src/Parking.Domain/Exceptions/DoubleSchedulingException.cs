﻿namespace Domain.Exceptions
{
    public class DoubleSchedulingException : DomainException
    {
        public DoubleSchedulingException() : base("SPOT-SCHEDULE-01")
        {
        }
    }
}
