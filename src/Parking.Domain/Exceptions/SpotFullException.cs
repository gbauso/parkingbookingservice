﻿namespace Domain.Exceptions
{
    public class SpotFullException : DomainException
    {
        public SpotFullException() : base("SPOT-SCHEDULE-02")
        {

        }
    }
}
