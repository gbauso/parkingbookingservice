﻿using CrossCutting.Exceptions;
using Domain.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Model
{
    public class Vehicle : IDomain
    {
        public Vehicle()
        {
                
        }

        public Vehicle(string plate)
        {
            Id = Guid.NewGuid();
            Plate = plate;
            Bookings = new HashSet<Booking>();
        }

        public Vehicle(Guid id, string plate, ICollection<Booking> bookings = default)
        {
            Id = id;
            Plate = plate;
            Bookings = bookings ?? new HashSet<Booking>();
        }

        public Guid Id { get; private set; }
        public string Plate { get; private set; }
        public virtual ICollection<Booking> Bookings { get; private set; }

        public void AddBooking(Booking booking)
        {
            booking.Validate();
            Bookings.Add(booking);
        }

        public void Validate()
        {
            var validator = new VehicleValidator();
            var validationResult = validator.Validate(this);

            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors.Select(i => i.ErrorCode));
        }
    }
}
