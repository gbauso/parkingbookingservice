﻿using CrossCutting.Exceptions;
using Domain.Exceptions;
using Domain.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Model
{
    public class Spot : IDomain
    {
        public Spot()
        {

        }

        public Spot(Guid id,string title, int capacity)
        {
            Id = id;
            Title = title;
            Capacity = capacity;
            Bookings = new HashSet<Booking>();
        }

        public Spot(string title, int capacity)
        {
            Id = Guid.NewGuid();
            Title = title;
            Capacity = capacity;
            Bookings = new HashSet<Booking>();
        }

        public Guid Id { get; private set; }
        public string Title { get; private set; }
        public int Capacity { get; private set; }

        public virtual ICollection<Booking> Bookings { get; private set; }

        public Booking Book(Vehicle vehicle, DateTime start, DateTime end)
        {
            vehicle.Validate();

            var bookingsAtPeriod = Bookings.Where(i => i.Start <= end && i.End >= start);  

            if (bookingsAtPeriod.Any(i => i.VehicleId == vehicle.Id)) throw new DoubleSchedulingException();
            if (bookingsAtPeriod.Count() >= Capacity) throw new SpotFullException();

            var booking = new Booking(this, vehicle, start, end);
            Bookings.Add(booking);
            vehicle.AddBooking(booking);

            return booking;
        }

        public void Validate()
        {
            var validator = new SpotValidator();
            var validationResult = validator.Validate(this);

            if (!validationResult.IsValid) 
                throw new ValidationException(validationResult.Errors.Select(i => i.ErrorCode));
        }
    }
}
