﻿using CrossCutting.Exceptions;
using Domain.Validators;
using System;
using System.Linq;

namespace Domain.Model
{
    public class Booking : IDomain
    {
        public Booking()
        {

        }
        public Booking(Spot spot, Vehicle vehicle, DateTime start, DateTime end)
        {
            Id = Guid.NewGuid();

            Spot = spot;
            SpotId = spot.Id;

            Vehicle = vehicle;
            VehicleId = vehicle.Id;

            BookingDate = DateTime.UtcNow;

            Start = start;
            End = end;

            MinutesBooked = (end - start).TotalMinutes;
        }

        public Guid Id { get; set; }
        public Guid SpotId { get; private set; }
        public Guid VehicleId { get; private set; }
        public virtual Spot Spot { get; private set; }
        public virtual Vehicle Vehicle { get; private set; }

        public DateTime BookingDate { get; set; }

        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        public double MinutesBooked { get; private set; }

        public void Validate()
        {
            var validator = new BookingValidator();
            var validationResult = validator.Validate(this);

            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors.Select(i => i.ErrorMessage));
        }
    }
}
