﻿using Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IVehicleRepository
    {
        Task Add(Vehicle vehicle);
        Task<Vehicle> FindVehicle(string plate);
        Task<IEnumerable<Vehicle>> GetVehicles(Func<Vehicle, bool> predicate);
    }
}
