﻿using Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ISpotRepository
    {
        Task<Spot> GetSpotInfo(Guid id);
        Task AddBooking(Booking booking);

    }
}
