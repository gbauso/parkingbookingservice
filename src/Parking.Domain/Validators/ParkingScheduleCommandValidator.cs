﻿using Domain.Service;
using FluentValidation;
using System;

namespace Domain.Validators
{
    public class ParkingScheduleCommandValidator : AbstractValidator<ParkingScheduleCommand>
    {
        public ParkingScheduleCommandValidator()
        {
            RuleFor(i => i.SpotId).NotNull().NotEqual(Guid.Empty).WithErrorCode("COMMAND-SPOT-01");
            RuleFor(i => i.VehiclePlate).NotNull().NotEqual(string.Empty).WithErrorCode("COMMAND-VHPLATE-01");
            RuleFor(i => i.Start).NotNull().GreaterThan(DateTime.Now).WithErrorCode("COMMAND-START-01");
            RuleFor(i => i.End).NotNull().GreaterThan(i => i.Start).WithErrorCode("COMMAND-END-01");
        }
    }
}
