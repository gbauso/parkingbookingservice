﻿using Domain.Model;
using FluentValidation;
using System;

namespace Domain.Validators
{
    public class BookingValidator : AbstractValidator<Booking>
    {
        public BookingValidator()
        {
            RuleFor(i => i.SpotId).NotEqual(Guid.Empty).NotNull().WithErrorCode("BOOKING-SPOT-01");
            RuleFor(i => i.VehicleId).NotEqual(Guid.Empty).NotNull().WithErrorCode("BOOKING-VEHICLE-01");
            RuleFor(i => i.Start).GreaterThan(DateTime.UtcNow).WithErrorCode("BOOKING-START-01");
            RuleFor(i => i.End).GreaterThan(i => i.Start).WithErrorCode("BOOKING-END-01");
            RuleFor(i => i.MinutesBooked).GreaterThan(0).WithErrorCode("BOOKING-MINUTES-02");
        }
    }
}
