﻿using Domain.Model;
using FluentValidation;
using System;

namespace Domain.Validators
{
    public class SpotValidator : AbstractValidator<Spot>
    {
        public SpotValidator()
        {
            RuleFor(i => i.Id).NotEqual(Guid.Empty).NotNull().WithErrorCode("COURSE_ID-01");
            RuleFor(i => i.Title).NotEqual(string.Empty).NotNull().WithErrorCode("COURSE_TITLE-01");
            RuleFor(i => i.Capacity).GreaterThan(0).WithErrorCode("COURSE_CAPACITY-01");
        }
    }
}
