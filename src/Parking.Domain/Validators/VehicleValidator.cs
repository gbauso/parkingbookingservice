﻿using Domain.Model;
using FluentValidation;
using System;

namespace Domain.Validators
{
    public class VehicleValidator : AbstractValidator<Vehicle>
    {
        public VehicleValidator()
        {
            RuleFor(i => i.Id).NotEqual(Guid.Empty).NotNull().WithErrorCode("VEHICLE-ID-01");
            RuleFor(i => i.Plate).NotNull().NotEqual(string.Empty).WithErrorCode("VEHICLE-PLATE-01");
        }
    }
}
