﻿using CrossCutting.Exceptions;
using Domain.Validators;
using MediatR;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Service
{
    public class ParkingScheduleCommand : IRequest<Guid>
    {
        public Guid SpotId { get; set; }
        public string VehiclePlate { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public async Task Validate()
        {
            var validator = new ParkingScheduleCommandValidator();
            var validationResult = await validator.ValidateAsync(this);

            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors.Select(i => i.ErrorCode));

        }
    }
}
