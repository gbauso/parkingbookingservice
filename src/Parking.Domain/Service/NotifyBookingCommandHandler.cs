﻿using Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Service
{
    public class NotifyBookingCommandHandler : IRequestHandler<NotifyBookingCommand, bool>
    {
        private readonly INotifier _Notifier;

        public NotifyBookingCommandHandler(INotifier notifier)
        {
            _Notifier = notifier;
        }

        public async Task<bool> Handle(NotifyBookingCommand request, CancellationToken cancellationToken)
        {
            await _Notifier.Notify(request.Notification);

            return true;
        }
    }
}
