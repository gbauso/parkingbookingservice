﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Domain.Model;
using CrossCutting.Exceptions;
using Domain.Interfaces;
using CrossCutting.ServiceBus;
using System;

namespace Domain.Service
{
    public class ParkingScheduleCommandHandler : IRequestHandler<ParkingScheduleCommand, Guid>
    {
        private readonly IVehicleRepository _VehicleRepository;
        private readonly ISpotRepository _SpotRepository;
        private readonly IUnitOfWork _UnitOfWork;
        private readonly IQueuePublisher _Publisher;

        public ParkingScheduleCommandHandler(IVehicleRepository vehicleRepository,
                                             ISpotRepository spotRepository,
                                             IUnitOfWork unitOfWork,
                                             IQueuePublisher publisher)
        {
            _VehicleRepository = vehicleRepository;
            _SpotRepository = spotRepository;
            _UnitOfWork = unitOfWork;
            _Publisher = publisher;
        }

        public async Task<Guid> Handle(ParkingScheduleCommand request, CancellationToken cancellationToken)
        {
            var spot = await _SpotRepository.GetSpotInfo(request.SpotId);
            if (spot == default)
                throw new ElementNotFoundException();

            var vehicle = await _VehicleRepository.FindVehicle(request.VehiclePlate);
            if (vehicle == default)
            {
                vehicle = new Vehicle(request.VehiclePlate);
                await _VehicleRepository.Add(vehicle);
            }

            var successfulyBooking = true;
            try
            {
                var booking = spot.Book(vehicle, request.Start, request.End);

                await _SpotRepository.AddBooking(booking);
                await _UnitOfWork.Commit();

                return booking.Id;
            }
            catch
            {
                successfulyBooking = false;
                throw;
            }
            finally
            {
                await _Publisher.Publish(new BusMessage("NotifyBooking",
                                                        new { Notification = new EmailNotification(successfulyBooking) }),
                                         "booking.queue");
            }


        }
    }
}
