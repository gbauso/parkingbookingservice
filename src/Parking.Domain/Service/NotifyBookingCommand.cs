﻿using MediatR;

namespace Domain.Service
{
    public class NotifyBookingCommand : IRequest<bool>
    {
        public EmailNotification Notification { get; set; }
    }
}
