﻿using Infrastructure.Database.Query;
using Infrastructure.Database.Query.Model;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Query
{
    public class ReportQueryHandler : IRequestHandler<ReportQuery, IEnumerable<Vehicle>>
    {
        private readonly IReadDatabase<Vehicle> _ReadDatabase;

        public ReportQueryHandler(IReadDatabase<Vehicle> readDatabase)
        {
            _ReadDatabase = readDatabase;
        }

        public async Task<IEnumerable<Vehicle>> Handle(ReportQuery request, CancellationToken cancellationToken)
        {
            var list = await _ReadDatabase.GetItems(new[] { "id", "Capacity", "Title", "MaxAge", "MinAge", "AvgAge", "EnrollmentCount" });

            return list as IEnumerable<Vehicle>;
        }
    }
}   
