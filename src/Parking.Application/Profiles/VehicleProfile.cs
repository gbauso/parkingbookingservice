﻿using AutoMapper;
using Domain.Model;
using QueryModel = Infrastructure.Database.Query.Model;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Parking.Application.Profiles
{
    public class VehicleProfile : Profile
    {
        public VehicleProfile()
        {
            CreateMap<Vehicle, QueryModel.Vehicle>()
                .ForMember(i => i.AvgTime, f => f.MapFrom(m => m.Bookings.DefaultIfEmpty().Average(i => i.MinutesBooked) / 60))
                .ForMember(i => i.MinTime, f => f.MapFrom(m => m.Bookings.DefaultIfEmpty().Min(i => i.MinutesBooked) / 60))
                .ForMember(i => i.MaxTime, f => f.MapFrom(m => m.Bookings.DefaultIfEmpty().Max(i => i.MinutesBooked) / 60));
        }
    }
}
