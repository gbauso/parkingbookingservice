﻿using Application.Validator;
using CrossCutting.Exceptions;
using MediatR;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Command
{
    public class BookingRequestCommand : IRequest<bool>
    {
        public Guid SpotId { get; set; }
        public string VehiclePlate { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public async Task Validate()
        {
            var validator = new BookingRequestCommandValidator();
            var validationResult = await validator.ValidateAsync(this);

            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors.Select(i => i.ErrorCode));
        }
    }
}
