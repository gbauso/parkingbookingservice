﻿using AutoMapper;
using Domain.Interfaces;
using Infrastructure.Database.Query;
using Infrastructure.Database.Query.Model;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command
{
    public class SyncDatabaseCommandHandler : IRequestHandler<SyncDatabaseCommand, bool>
    {
        private readonly IVehicleRepository _VehicleRepository;
        private readonly IReadDatabase<Vehicle> _VehicleQuery;
        private readonly IMapper _Mapper;

        public SyncDatabaseCommandHandler(IVehicleRepository vehicleRepository, IReadDatabase<Vehicle> vehicleQuery, IMapper mapper)
        {
            _VehicleRepository = vehicleRepository;
            _VehicleQuery = vehicleQuery;
            _Mapper = mapper;
        }

        public async Task<bool> Handle(SyncDatabaseCommand request, CancellationToken cancellationToken)
        {
            var reportIds = _VehicleQuery.GetItems(new[] { "id" }).Result.Select(i => Guid.Parse(i.Id));

            var syncVehicles = await _VehicleRepository
                                            .GetVehicles(i => i.Bookings.Any(b => b.BookingDate >= request.LastExecution)
                                                              || !reportIds.Contains(i.Id)
                                                         );

            foreach (var item in syncVehicles)
            {
                await _VehicleQuery.RemoveItem(item.Id.ToString());
                await _VehicleQuery.AddItem(_Mapper.Map<Vehicle>(item));
            }

            return true;
        }
    }
}
