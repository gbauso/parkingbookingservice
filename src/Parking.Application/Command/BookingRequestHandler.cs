﻿using System.Threading;
using System.Threading.Tasks;
using CrossCutting.ServiceBus;
using MediatR;

namespace Application.Command
{
    public class BookingRequestHandler : IRequestHandler<BookingRequestCommand, bool>
    {
        private readonly IQueuePublisher _Publisher;

        public BookingRequestHandler(IQueuePublisher publisher)
        {
            _Publisher = publisher;
        }

        public async Task<bool> Handle(BookingRequestCommand request, CancellationToken cancellationToken)
        {
            var message = new BusMessage("ParkingSchedule", request);

            await _Publisher.Publish(message, "booking.queue");

            return true;
        }
    }
}
