﻿using Application.Command;
using FluentValidation;
using System;

namespace Application.Validator
{
    public class BookingRequestCommandValidator : AbstractValidator<BookingRequestCommand>
    {
        public BookingRequestCommandValidator()
        {
            RuleFor(i => i.SpotId).NotNull().NotEqual(Guid.Empty).WithErrorCode("REQUEST-SPOT-01");
            RuleFor(i => i.VehiclePlate).NotNull().NotEqual(string.Empty).WithErrorCode("REQUEST-VHPLATE-01");
            RuleFor(i => i.Start).NotNull().GreaterThan(DateTime.Now).WithErrorCode("REQUEST-START-01");
            RuleFor(i => i.End).NotNull().GreaterThan(i => i.Start).WithErrorCode("REQUEST-END-01");
        }
    }
}
