﻿using Domain.Interfaces;
using System.Threading.Tasks;

namespace Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ParkDbContext _Context;

        public UnitOfWork(ParkDbContext context)
        {
            _Context = context;
        }

        public async Task Commit()
        {
            await _Context.SaveChangesAsync();
        }
    }
}
