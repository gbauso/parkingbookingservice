﻿using Domain.Interfaces;
using Domain.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Infrastructure.Repository
{
    public class VehicleRepository : IVehicleRepository
    {
        private readonly ParkDbContext _Context;

        public VehicleRepository(ParkDbContext courseDbContext)
        {
            _Context = courseDbContext;
        }

        public async Task Add(Vehicle student)
        {
            await _Context.Vehicles.AddAsync(student);
        }

        public async Task<Vehicle> FindVehicle(string plate)
        {
            return await _Context
                                .Vehicles
                                .FirstOrDefaultAsync(i =>
                                    i.Plate.ToLower() == plate.ToLower()
                                 );
        }

        public Task<IEnumerable<Vehicle>> GetVehicles(Func<Vehicle, bool> predicate)
        {
            return Task.FromResult(_Context
                                .Vehicles
                                .Include(i => i.Bookings)
                                .Where(predicate));
        }
    }
}
