﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Model;

namespace Infrastructure.Repository
{
    public class SpotRepository : ISpotRepository
    {
        private readonly ParkDbContext _Context;

        public SpotRepository(ParkDbContext SpotDbContext)
        {
            _Context = SpotDbContext;
        }

        public async Task AddBooking(Booking booking)
        {
            await _Context.Bookings.AddAsync(booking);
        }

        public async Task<Spot> GetSpotInfo(Guid id)
        {
            return await _Context
                                .Spots
                                .Include(i => i.Bookings)
                                    .ThenInclude(e => e.Vehicle)
                                .FirstOrDefaultAsync(i => i.Id == id);
        }

    }
}
