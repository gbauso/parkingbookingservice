﻿using Microsoft.EntityFrameworkCore;
using Domain.Model;

namespace Infrastructure
{
    public class ParkDbContext : DbContext
    {
        public ParkDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Spot> Spots { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Booking> Bookings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Spot>(cfg =>
            {
                cfg.HasData(new Spot("Martin Av.", 5));
                cfg.ToTable("Spot");
                cfg.HasKey(i => i.Id);
                cfg.Property(i => i.Title).HasMaxLength(75).IsRequired();
                cfg.HasMany(i => i.Bookings).WithOne(i => i.Spot);
            });

            modelBuilder.Entity<Vehicle>(cfg =>
            {
                cfg.ToTable("Vehicle");
                cfg.HasKey(i => i.Id);
                cfg.Property(i => i.Plate).HasMaxLength(40).IsRequired();
                cfg.HasMany(i => i.Bookings).WithOne(i => i.Vehicle);
            });


            modelBuilder.Entity<Booking>(cfg =>
            {
                cfg.ToTable("Booking");
                cfg.HasIndex(i => new { i.SpotId, i.VehicleId, i.Start, i.End }).IsUnique();
            });
                
                
        }
    }
}
