﻿using Newtonsoft.Json;

namespace Infrastructure.Database.Query.Model
{
    public class Vehicle : IQueryModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string Plate { get; set; }

        public double AvgTime { get; set; }
        public double MinTime { get; set; }
        public double MaxTime { get; set; }

    }
}
