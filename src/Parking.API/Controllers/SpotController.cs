﻿using System;
using System.Threading.Tasks;
using Application.Command;
using Application.Query;
using Infrastructure.Database.Query.Model;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpotController : ControllerBase
    {
        private readonly IMediator _Mediator;

        public SpotController(IMediator mediator)
        {
            _Mediator = mediator;
        }

        [HttpPost]
        [Route("book")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string[]))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> BookingPark([FromBody] BookingRequestCommand command)
        {
            await command.Validate();
            await _Mediator.Send(command);

            return Accepted();
        }

        
    }
}