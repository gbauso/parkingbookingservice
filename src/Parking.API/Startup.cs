using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MediatR;
using AutoMapper;
using API.Filter;
using Microsoft.OpenApi.Models;
using Application.Command;
using CrossCutting.ServiceBus;
using Infrastructure.ServiceBus.Azure;
using Domain.Interfaces;
using Infrastructure.Repository;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Application.Query;
using Infrastructure.Database.Query;
using Infrastructure.Database.Query.Model;
using Parking.Application.Profiles;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(cfg =>
            {
                cfg.Filters.Add(typeof(ErrorHandlingFilter));
            });

            services.AddMediatR(typeof(BookingRequestHandler),
                                typeof(SyncDatabaseCommandHandler),
                                typeof(ReportQueryHandler));

            services.AddAutoMapper(typeof(VehicleProfile));

            services.AddSingleton<IQueuePublisher, AzurePublisher>();

            services.AddDbContext<ParkDbContext>(cfg =>
            {
                cfg.UseSqlServer(Configuration.GetConnectionString("ParkDbContext"),
                    ac => ac.MigrationsAssembly("Parking.Infrastructure"));
            });

            services.AddScoped<IVehicleRepository, VehicleRepository>();
            services.AddScoped<IReadDatabase<Vehicle>, CosmosDbManager<Vehicle>>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v2", new OpenApiInfo { Title = "Parking", Version = "v2" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "Parking V2");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}